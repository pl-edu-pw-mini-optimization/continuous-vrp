﻿using CommandLine;
using CommandLine.Text;
using ContVRP;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using VRP.Core.Locations;
using VRP.Core.TSPAlgorithms;
using VRP.Core.VRPAlgorithms;
using VRP.Core.VRPConstraints;
using VRP.Exceptions;
using VRP.Providers;
using VRP.Providers.VRPLIB;

namespace UchoaTester
{
    class Program
    {
        public class Options
        {
            [Option('k', "clusters-per-vehicle",
                Default = 1,
                HelpText = "Number of maximum requests clusters assigned to single vehicle.")]
            public int K { get; set; }
            [Option('i', "iterations",
                Default = 10,
                HelpText = "Number of PSO iterations.")]
            public int Iterations { get; set; }
            [Option('s', "swarm-size",
                Default = 10,
                HelpText = "Number of PSO particles.")]
            public int Particles { get; set; }

            [Option('r', "repetitions",
                Default = 5,
                HelpText = "Number of algorithm restarts from scratch.")]
            public int Repetitions { get; set; }

            [Option("skip",
                Default = 0,
                HelpText = "Number of benchmarks to skip.")]
            public int SkipBenchmarks { get; set; }

            [Option('a', "algorithm",
                Default = 2,
                HelpText = "Optimization algorithm (0-Greedy,1-TreeCluster,2-ContinuousCluster,3-ContinuousClusterForMatrix).")]
            public int Algorithm { get; set; }

            [Option('b', "benchmarks",
                Default = "../VRP/Instances/Warsaw",
                HelpText = "Location with benchmarks")]
            public string BenchmarksDirectory { get; set; }

            [Option('c', "compression",
                Default = false,
                HelpText = "Apply street compression - needs map data in benchmark")]
            public bool ApplyCompression { get; set; }

            [Usage(ApplicationAlias = "dotnet run UchoaTester.dll")]
            public static IEnumerable<Example> Examples
            {
                get
                {
                    yield return new Example("fast scenario", new Options { K = 1, Iterations = 10, Particles = 10, Repetitions = 1 });
                    yield return new Example("typical scenario", new Options { K = 1, Iterations = 200, Particles = 20, Repetitions = 10 });
                }
            }

        }


        private static Options ParseRuntimeOptions(string[] args)
        {
            ParserResult<Options> parsed = Parser.Default.ParseArguments<Options>(args);
            if (parsed.Tag == ParserResultType.NotParsed)
            {
                NotParsed<Options> notparsed = (CommandLine.NotParsed<Options>)parsed;
                if (!notparsed.Errors.Any(err => err.Tag == ErrorType.VersionRequestedError || err.Tag == ErrorType.HelpRequestedError))
                {
                    /*
                    foreach (var error in
                        notparsed
                        .Errors
                        .Where(err =>
                            err.Tag != ErrorType.VersionRequestedError &&
                            err.Tag != ErrorType.HelpRequestedError))
                    {
                        var info = (error is MissingRequiredOptionError) ?
                            (error as MissingRequiredOptionError).NameInfo.LongName : "";
                    logger.LogCritical($"{error.Tag} {info}");
                    }
                    */
                }
                if (notparsed.Errors.Any(err => err.StopsProcessing || err.Tag == ErrorType.MissingRequiredOptionError))
                {
                    Environment.Exit(1);
                }
            }
            Options options = (Options)parsed.MapResult(
                (Options opt) => opt,
                _ => Nothing());
            return options;
        }


        private static object Nothing()
        {
            return null;
        }

        static void Main(string[] args)
        {
            Options options = ParseRuntimeOptions(args);
            var fileList = Directory.EnumerateFiles(options.BenchmarksDirectory);
            var fileInfos = fileList
                .Select(fn => new FileInfo(fn))
                .Where(fi => fi.Extension.EndsWith("vrp"))
                .Skip(options.SkipBenchmarks);
            foreach (var fileInfo in fileInfos)
            {
                var start = DateTime.Now;
                IAllDataProvider provider = ProviderFactory.GetProviderForFile(fileInfo, new TwoOptRouteFactory(), options.ApplyCompression);
                IAllDataProvider basicProvider = ProviderFactory.GetProviderForFile(fileInfo, new TwoOptRouteFactory(), false);
                if (provider == null)
                    continue;
                List<double> values = new List<double>();
                for (int repeat = 0; repeat < options.Repetitions; ++repeat)
                {
                    try
                    {
                        var localStart = DateTime.Now;
                        VRP.Core.VRPAlgorithms.VRPAlgorithm vrp = SelectOptimizationAlgorithm(options, provider, basicProvider);
                        vrp.Optimize();
                        var localEnd = DateTime.Now;
                        LogResult(fileInfo, repeat, vrp, localStart, localEnd);

                        var length = vrp.TotalLength;
                        values.Add(length);
                        if (vrp.Deterministic) break;
                    }
                    catch (Exception ex)
                    {
                        Console.Error.Write(ex.StackTrace);
                        continue;
                    }
                }
                var end = DateTime.Now;
                if (values.Any())
                {
                    Console.Write(fileInfo.Name);
                    Console.Write(",");
                    Console.Write(values.Min());
                    Console.Write(",");
                    Console.WriteLine(Math.Round((end - start).TotalMilliseconds));
                }
            }

        }

        private static VRPAlgorithm SelectOptimizationAlgorithm(Options options, IAllDataProvider provider, IAllDataProvider basicProvider)
        {
            var heuristics = new List<VRPAlgorithm>()
                                    {
                                        new GreedyVRP(basicProvider, basicProvider, basicProvider),
                                        new TreeClusteringVRP(basicProvider, basicProvider, basicProvider)
                                    };
            switch (options.Algorithm)
            {
                case 0:
                    return new GreedyVRP(provider, provider, provider);
                case 1:
                    return new TreeClusteringVRP(provider, provider, provider);
                case 2:
                    return new ContinuousVRPWithExplicitCapacityClustering(
                        options.Particles, options.Iterations, options.K, heuristics, provider);
                case 3:
                    return new ContinuousVRPWithExplicitCapacityClusteringAndDistance(
                        options.Particles, options.Iterations, options.K, heuristics, provider);
                case 4:
                    return new GreedyWithSizePriorityWhenEmpty(provider, provider, provider);
                default:
                    throw new ArgumentOutOfRangeException(string.Format("No such algorithm: {0}", options.Algorithm));
            }
        }

        private static void LogResult(FileInfo fileInfo, int repeat, VRP.Core.VRPAlgorithms.VRPAlgorithm vrp, DateTime localStart, DateTime localEnd)
        {
            var outputFolder = string.Format("{0}", "Output");
            if (!(new DirectoryInfo(outputFolder)).Exists)
            {
                Directory.CreateDirectory(outputFolder);
            }
            var benchmarkFolder = string.Format("{0}/{1}", outputFolder, fileInfo.Name.Replace(fileInfo.Extension,""));
            if (!(new DirectoryInfo(benchmarkFolder).Exists))
            {
                Directory.CreateDirectory(benchmarkFolder);
            }
            var algorithmFolder = string.Format("{0}/{1}", benchmarkFolder, vrp.Name);
            if (!(new DirectoryInfo(algorithmFolder).Exists))
            {
                Directory.CreateDirectory(algorithmFolder);
            }

            List<string> lines = new List<string>();
            var length = vrp.TotalLength;
            List<Vehicle> routes = vrp.GetSolution();

            foreach (Vehicle vhcl in routes)
            {
                lines.Add(string.Join(" ", vhcl.Schedule.Select(loc => loc.Id)));
            }
            lines.Add("Cost " + length);
            File.WriteAllLines(string.Format("{0}/{1}-{2}.{3}", algorithmFolder, fileInfo.Name.Replace(fileInfo.Extension, ""), repeat, "sol"), lines);

            lines.Clear();
            lines.Add(string.Format(CultureInfo.InvariantCulture, "{0},{1},{2},{3}", "Vehicle", "Id", "X", "Y"));
            foreach (Vehicle vhcl in routes)
            {
                foreach (Location loc in vhcl.Schedule)
                {
                    lines.Add(string.Format(CultureInfo.InvariantCulture, "{0},{1},{2},{3}", routes.IndexOf(vhcl) + 1, loc.Id + 1, loc.X, loc.Y));
                }
            }
            File.WriteAllLines(string.Format("{0}/{1}-{2}.{3}", algorithmFolder, fileInfo.Name.Replace(fileInfo.Extension, ""), repeat, "route"), lines);

            var singleResultsFile = string.Format("{0}/{1}", outputFolder, "results.csv");
            if (!(new FileInfo(singleResultsFile)).Exists)
            {
                File.WriteAllLines(singleResultsFile, new string[] {
                            string.Format("{0},{1},{2},{3},{4}",
                            "Benchmark",
                            "Algorithm",
                            "ExperimentNo",
                            "Result",
                            "Time")
                        });
            }
            File.AppendAllLines(singleResultsFile, new string[] {
                            string.Format("{0},{1},{2},{3},{4}",
                            fileInfo.Name.Replace(fileInfo.Extension,""),
                            vrp.Name,
                            repeat,
                            length,
                            Math.Round((localEnd - localStart).TotalMilliseconds))
                        });
        }
    }
}