﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core;
using VRP.Core.Locations;
using VRP.Core.TSPAlgorithms;
using VRP.Providers;
using VRP.Test.Providers;

namespace VRP.Test.Mock
{
    public class RouteWithZigZag : TwoOptRoute
    {
        public RouteWithZigZag() : base(new SimpleLocation(0, 0, 0, 0), new SimpleLocation(0, 0, 0, 0), new EuclideanProvider())
        {
            var SimpleLocation1 = new SimpleLocation(1, 1, 1, 1);
            var insertionCost1 = ComputeInsertionCost(SimpleLocation1, 1);
            Insert(SimpleLocation1, insertionCost1, 1);
            var SimpleLocation2 = new SimpleLocation(2, 0, 1, 1);
            var insertionCost2 = ComputeInsertionCost(SimpleLocation2, 2);
            Insert(SimpleLocation2, insertionCost2, 2);
            var SimpleLocation3 = new SimpleLocation(3, 1, 0, 1);
            var insertionCost3 = ComputeInsertionCost(SimpleLocation3, 3);
            Insert(SimpleLocation3, insertionCost3, 3);
        }
    }
}
