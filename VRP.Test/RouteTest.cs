using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VRP.Test.Providers;
using VRP.Exceptions;
using System.Collections.Generic;
using VRP.Core;
using VRP.Test.Mock;
using VRP.Core.TSPAlgorithms;
using VRP.Core.Locations;

namespace VRP.Test
{
    [TestClass]
    public class RouteTest
    {
        [TestMethod]
        public void LocationInsertTest()
        {
            var provider = new VRPPFourByFourMatrixProvider();
            List<Location> locations = provider.GetLocations();
            Location depot = provider.GetDepot();
            Location request1 = locations[0];
            Location request2 = locations[1];
            Location request3 = locations[2];
            FixedEndsRoute route = new NoFurtherOptRoute(depot, depot, provider);
            Assert.AreEqual(0.0, route.Length);

            bool insertStatus1 = route.TryInsert(request1, out double insertionCost1);
            Assert.AreEqual(2.01, insertionCost1);
            Assert.AreEqual(route, request1.Route);
            Assert.IsTrue(insertStatus1);

            bool insertStatus2 = route.TryInsert(request2, out double insertionCost2);
            Assert.AreEqual(Math.Sqrt(2) - 0.01, insertionCost2, 1e-6);
            Assert.AreEqual(route, request2.Route);
            Assert.IsTrue(insertStatus2);

            double insertionCost3 = route.ComputeLowestCostInsertionPlace(request3, out int index);
            Assert.AreEqual(2.0 - Math.Sqrt(2), insertionCost3, 1e-6);
            Assert.AreEqual(3, index);
            Assert.IsNull(request3.Route);
        }

        private class InsertionWrapper
        {
            private FixedEndsRoute route;
            private Location location;
            public double InsertionCost { get; private set; }
            public double RemovalCost { get; private set; }

            public InsertionWrapper(FixedEndsRoute route, Location location)
            {
                this.route = route;
                this.location = location;
            }

            public void TryInsert()
            {
                route.TryInsert(location, out double insertionCost);
                InsertionCost = insertionCost;
            }
            public void TryRemove()
            {
                route.Remove(location, out double removalCost);
                RemovalCost = removalCost;
            }
        }

        [TestMethod]
        public void LocationRemoveTest()
        {
            var provider = new VRPPFourByFourMatrixProvider();
            List<Location> locations = provider.GetLocations();
            Location depot = provider.GetDepot();
            Location request1 = locations[0];
            Location request2 = locations[1];
            Location request3 = locations[2];
            FixedEndsRoute route1 = new TwoOptRoute(depot, depot, provider);
            route1.TryInsert(request1, out double insertionCost1_1);
            route1.TryInsert(request2, out double insertionCost1_2);

            FixedEndsRoute route2 = new TwoOptRoute(depot, depot, provider);
            route2.TryInsert(request3, out double insertionCost2_3);
            var length = route2.Length;
            var insertionWrapper = new InsertionWrapper(route2, request2);
            Assert.ThrowsException<AssignedLocationException>((Action)insertionWrapper.TryInsert);
            Assert.AreEqual(length, route2.Length);
            Assert.AreEqual(route1, request2.Route);

            var removeStatus1_2 = route1.Remove(request2, out double removalCost1_2);
            Assert.IsNull(request2.Route);
            Assert.AreEqual(2.01, route1.Length);
            Assert.IsTrue(removeStatus1_2);
            route2.TryInsert(request2, out double insertionCost2_2);
            Assert.AreEqual(2 + Math.Sqrt(2), route2.Length);
            Assert.AreEqual(route2, request2.Route);

            var removeStatus1_3 = route1.Remove(request3, out double removalCost1_3);
            Assert.IsNotNull(request3.Route);
            Assert.AreEqual(0.0, removalCost1_3);
            Assert.IsFalse(removeStatus1_3);
        }

        [TestMethod]
        public void OptimizeTest()
        {
            FixedEndsRoute route = new ShortestEntangledRoute();
            Assert.AreEqual(1 + 2 * Math.Sqrt(2), route.Length, 1e-4);
            route.Optimize();
            Assert.AreEqual(3.0, route.Length, 1e-4);

            var zigzagroute = new RouteWithZigZag();
            Assert.AreEqual(2 + 2*Math.Sqrt(2), zigzagroute.Length, 1e-4);
            zigzagroute.Optimize();
            Assert.AreEqual(4, zigzagroute.Length, 1e-4);
        }
    }
}
