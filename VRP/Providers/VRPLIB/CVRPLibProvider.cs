﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core;
using VRP.Core.Locations;
using VRP.Core.TSPAlgorithms;
using VRP.Exceptions;

namespace VRP.Providers.VRPLIB
{
    public class CVRPLibProvider : BaseFileProvider
    {
        const string EUC_2D = "EUC_2D";

        protected override int XCoordinate => 1;
        protected override int YCoordinate => 2;


        public CVRPLibProvider(string filename, RouteFactory routeFactory) : base(filename, routeFactory)
        {
        }

        protected override void CheckEdgeType(string[] edgeInfo)
        {
            if (edgeInfo[1] != EUC_2D)
                throw new FileFormatException("Unsupported edge type " + edgeInfo[1]);
        }
        public override double GetDistance(Location from, Location to)
        {
            return Math.Round(Math.Sqrt((from.X - to.X) * (from.X - to.X) +
                (from.Y - to.Y) * (from.Y - to.Y)));
        }

    }
}
