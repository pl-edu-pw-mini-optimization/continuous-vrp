﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using VRP.Core;
using VRP.Core.Locations;
using VRP.Core.TSPAlgorithms;
using VRP.Core.VRPConstraints;
using VRP.Exceptions;

namespace VRP.Providers.VRPLIB
{
    public abstract class BaseFileProvider : IAllDataProvider
    {
        const string DEPOT_SECTION = "DEPOT_SECTION";
        const string EOF = "EOF";
        const string DEMAND_SECTION = "DEMAND_SECTION";
        const string NODE_COORD_SECTION = "NODE_COORD_SECTION";
        const string CAPACITY = "CAPACITY";
        const string EDGE_WEIGHT_TYPE = "EDGE_WEIGHT_TYPE";
        const string DIMENSION = "DIMENSION";
        const string NAME = "NAME";
        const string TYPE = "TYPE";
        const string CVRP = "CVRP";
        const string COMMENT = "COMMENT";
        protected const char ID_DATA_SEPARTOR = '\t';
        protected const char DATA_SEPARATOR = ' ';
        protected const char DEFINITION_SEPARATOR = ':';
        protected readonly int clientCount;
        private readonly int vehicleCapacity;
        private Location[] clients;
        private double[][] clientsLocationDefinitions;
        private readonly RouteFactory routeFactory;

        protected abstract int YCoordinate { get; }
        protected abstract int XCoordinate { get; }

        public abstract double GetDistance(Location from, Location to);
        protected abstract void CheckEdgeType(string[] edgeInfo);


        public List<Location> GetLocations()
        {
            return clients.Skip(1)
                .Select(loc => loc.Copy())
                .ToList();
        }

        public BaseFileProvider(string filename, RouteFactory routeFactory)
        {
            this.routeFactory = routeFactory;
            string[] fileLines = File.ReadAllLines(filename);
            for (int i = 0; i < fileLines.Length; i++)
            {
                if (fileLines[i].TrimStart().StartsWith(DIMENSION))
                {
                    string[] dimensionInfo = GetProperty(fileLines, i);
                    clientCount = int.Parse(dimensionInfo[1]);
                }
                if (fileLines[i].TrimStart().StartsWith(CAPACITY))
                {
                    string[] capacityInfo = GetProperty(fileLines, i);
                    vehicleCapacity = int.Parse(capacityInfo[1]);
                }
                i = CheckingSections(fileLines, i);
            }
        }

        protected virtual int CheckingSections(string[] fileLines, int i)
        {
            if (fileLines[i].TrimStart().StartsWith(EDGE_WEIGHT_TYPE))
            {
                string[] edgeInfo = GetProperty(fileLines, i);
                CheckEdgeType(edgeInfo);
            }
            if (fileLines[i].TrimStart().StartsWith(TYPE))
            {
                string[] typeInfo = GetProperty(fileLines, i);
                CheckProblemType(typeInfo);
            }
            if (fileLines[i].TrimStart().StartsWith(NODE_COORD_SECTION))
            {
                i = GettingLocations(fileLines, i);
            }
            if (fileLines[i].TrimStart().StartsWith(DEMAND_SECTION))
            {
                i = GettingDemands(fileLines, i);
            }

            return i;
        }

        protected virtual void CheckProblemType(string[] typeInfo)
        {
            if (typeInfo[1] != CVRP)
                throw new FileFormatException("Unsupported problem type " + typeInfo[1]);
        }

        protected static string[] GetProperty(string[] fileLines, int i)
        {
            return fileLines[i].Split(new char[] { DATA_SEPARATOR, DEFINITION_SEPARATOR, ID_DATA_SEPARTOR }, StringSplitOptions.RemoveEmptyEntries);
        }

        private int GettingDemands(string[] fileLines, int i)
        {
            clients = new Location[clientCount];
            i++;
            while (fileLines[i].Split(new char[] { DATA_SEPARATOR, ID_DATA_SEPARTOR },
                StringSplitOptions.RemoveEmptyEntries).Length == 2)
            {
                string[] data = fileLines[i].Split(new char[] { DATA_SEPARATOR, ID_DATA_SEPARTOR },
                StringSplitOptions.RemoveEmptyEntries);
                if (data.Length == 2)
                {
                    //HACK: assumption about IDs starting from 1
                    int id = int.Parse(data[0]) - 1;
                    clients[id] = new SimpleLocation(
                        id,
                        clientsLocationDefinitions[id][0],
                        clientsLocationDefinitions[id][1],
                        double.Parse(data[1], CultureInfo.InvariantCulture));
                }
                i++;
            }
            i--;
            return i;
        }

        private int GettingLocations(string[] fileLines, int i)
        {
            clientsLocationDefinitions = new double[clientCount][];
            i++;
            while (fileLines[i].Split(new char[] { DATA_SEPARATOR, ID_DATA_SEPARTOR },
                StringSplitOptions.RemoveEmptyEntries).Length == 3)
            {
                string[] data = fileLines[i].Split(ID_DATA_SEPARTOR, DATA_SEPARATOR);
                if (data.Length == 3)
                {
                    //HACK: assumption about client IDs starting from 1
                    int id = int.Parse(data[0]) - 1;
                    clientsLocationDefinitions[id] = new double[2];
                    clientsLocationDefinitions[id][0] = double.Parse(data[XCoordinate], CultureInfo.InvariantCulture);
                    clientsLocationDefinitions[id][1] = double.Parse(data[YCoordinate], CultureInfo.InvariantCulture);
                }
                i++;
            }
            i--;
            return i;
        }

        public List<Vehicle> GetVehicles()
        {
            var vehicles = new List<Vehicle>();
            for (int i = 0; i < clientCount; i++)
            {
                vehicles.Add(new SingleRouteVehicle(
                    vehicleCapacity,
                    clients[0],
                    clients[0],
                    this,
                    routeFactory));
            }
            return vehicles;
        }

        public List<double> GetCapacities()
        {
            return new List<double>() { vehicleCapacity };
        }

        public Location GetDepot()
        {
            return clients[0].Copy();
        }
    }
}
