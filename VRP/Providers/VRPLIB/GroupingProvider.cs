﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRP.Core.Locations;
using VRP.Core.VRPConstraints;

namespace VRP.Providers.VRPLIB
{
    public class GroupingProvider : IAllDataProvider
    {
        private IAllDataProvider innerProvider;
        private List<GroupedLocation> paths;

        public GroupingProvider(IAllDataProvider provider, IClassifiedLocationsProvider classifiedLocationsProvider, IDistancesAndHopsProvider distanceAndHopsProvider)
        {
            innerProvider = provider;
            var classLocations = classifiedLocationsProvider.GetClassifiedLocations();
            var groupedLocations = classLocations
                .GroupBy(loc => loc.ClassId)
                .ToDictionary(gloc => gloc.Key, gloc => gloc.ToList());
            paths = new List<GroupedLocation>();
            foreach (var classId in groupedLocations.Keys)
            {
                var roughGroup = groupedLocations[classId];
                var tempPaths = new List<GroupedLocation>();
                foreach (var location in roughGroup)
                {
                    var selectedGroup = tempPaths.FirstOrDefault(gloc => distanceAndHopsProvider.GetHops(gloc, location) == 0
                        || distanceAndHopsProvider.GetHops(location, gloc) == 0);
                    if (selectedGroup == null)
                    {
                        tempPaths.Add(new GroupedLocation(location, distanceAndHopsProvider));
                    }
                    else
                    {
                        selectedGroup.Insert(location);
                    }
                }
                foreach (var tempPath in tempPaths)
                {
                    paths.AddRange(tempPath.Divide(GetCapacities()[0]));
                }
            }
        }

        public List<double> GetCapacities()
        {
            return innerProvider.GetCapacities();
        }

        public Location GetDepot()
        {
            return innerProvider.GetDepot();
        }

        public double GetDistance(Location from, Location to)
        {
            return innerProvider.GetDistance(from, to);
        }

        public List<Location> GetLocations()
        {
            return paths.Select(loc => loc.Copy() as Location).ToList();
        }

        public List<Vehicle> GetVehicles()
        {
            return innerProvider.GetVehicles();
        }
    }
}
