﻿using System.Collections.Generic;
using VRP.Core.VRPConstraints;

namespace VRP.Providers
{
    public interface IVehiclesProvider
    {
        List<Vehicle> GetVehicles();
        List<double> GetCapacities();
    }
}
