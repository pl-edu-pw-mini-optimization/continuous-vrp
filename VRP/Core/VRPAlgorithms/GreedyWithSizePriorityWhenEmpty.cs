﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRP.Core.Locations;
using VRP.Core.VRPConstraints;
using VRP.Providers;

namespace VRP.Core.VRPAlgorithms
{
    public class GreedyWithSizePriorityWhenEmpty : GreedyVRP
    {
        public GreedyWithSizePriorityWhenEmpty(ILocationsProvider locationsProvider, IDistancesProvider distancesProvider, IVehiclesProvider vehiclesProvider)
            : base(locationsProvider, distancesProvider, vehiclesProvider)
        {
        }

        protected override void InsertGreedyLocationVehicleMatch(List<Location> locations, List<Vehicle> consideredVehicles)
        {
            var templocations = locations.OrderByDescending(loc => loc.Size).ToList();
            locations.Clear();
            locations.AddRange(templocations);
            base.InsertGreedyLocationVehicleMatch(
                locations,
                consideredVehicles);
        }

        public override string Name => base.Name + "SizePriority";


    }
}
