﻿using System;
using System.Collections.Generic;
using System.Linq;
using VRP.Exceptions;
using VRP.Providers;
using VRP.Utils.Clustering;

namespace VRP.Core.VRPAlgorithms
{
    public class TreeClusteringVRP : VRPAlgorithm
    {
        public override string Name => "TreeCluster";

        public override bool Deterministic => true;

        public TreeClusteringVRP(ILocationsProvider locationsProvider, IDistancesProvider distancesProvider, IVehiclesProvider vehiclesProvider) : base(locationsProvider, distancesProvider, vehiclesProvider)
        {
        }

        public override void Optimize()
        {
            List<double> capacities = VehiclesProvider.GetCapacities();
            if (capacities.Count != 1)
                throw new FileFormatException("Problem is not homogeneous");
            double capacity = capacities[0];
            List<ClusteredLocation> clusteredLocations = new List<ClusteredLocation>();
            Locations.ForEach(
                loc => loc.ClearRoute());
            Locations.ForEach(
                loc => clusteredLocations.Add(new ClusteredLocation(loc)));
            var edges = GraphMSTClusterer.ComputeOrderedEdges(DistancesProvider, clusteredLocations, clusteredLocations);
            var graphMSTClusterer = new GraphMSTClusterer(capacity, clusteredLocations, edges);
            var clusters = graphMSTClusterer.Clusters;
            int vehicleId = 0;
            foreach (Cluster cluster in clusters)
            {
                foreach (ClusteredLocation location in cluster.Locations)
                {
                    Vehicles[vehicleId].Insert(location.Location);
                    Locations.Remove(location.Location);
                }
                vehicleId++;
            }
            if (Locations.Any())
                throw new NoSolutionException("Tree clustering did not cluster all locations");
            Vehicles.ForEach(
                vhcl => vhcl.Optimize());
            if (Vehicles.Sum(vhcl => vhcl.TotalSize) != LocationsProvider.GetLocations().Sum(loc => loc.Size))
                throw new NoSolutionException("Tree clustering did not cluster all locations");
        }
    }

}
