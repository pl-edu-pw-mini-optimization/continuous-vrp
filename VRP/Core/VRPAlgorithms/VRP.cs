﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRP.Core.Locations;
using VRP.Core.VRPConstraints;
using VRP.Providers;

namespace VRP.Core.VRPAlgorithms
{
    public abstract class VRPAlgorithm
    {
        public ILocationsProvider LocationsProvider { get; private set; }
        public IDistancesProvider DistancesProvider { get; private set; }
        public IVehiclesProvider VehiclesProvider { get; private set; }

        public List<Vehicle> Vehicles { get; private set; }
        public List<Location> Locations { get; private set; }

        public abstract string Name { get; }

        public abstract bool Deterministic { get; }

        public virtual double TotalLength { get { return Vehicles.Sum(vhcl => vhcl.TotalLength); } }

        public VRPAlgorithm(ILocationsProvider locationsProvider, IDistancesProvider distancesProvider, IVehiclesProvider vehiclesProvider)
        {
            LocationsProvider = locationsProvider;
            DistancesProvider = distancesProvider;
            VehiclesProvider = vehiclesProvider;
            Vehicles = vehiclesProvider.GetVehicles();
            Locations = locationsProvider.GetLocations();
        }

        public List<Vehicle> GetSolution()
        {
            return Vehicles.Where(vhcl => vhcl.TotalLength > 0).ToList();
        }

        public abstract void Optimize();

    }
}
