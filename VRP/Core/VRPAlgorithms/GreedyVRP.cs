﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRP.Core.Locations;
using VRP.Core.VRPConstraints;
using VRP.Exceptions;
using VRP.Providers;

namespace VRP.Core.VRPAlgorithms
{
    public class GreedyVRP : VRPAlgorithm
    {
        public override string Name => "Greedy";

        public override bool Deterministic => true;

        public GreedyVRP(ILocationsProvider locationsProvider, IDistancesProvider distancesProvider, IVehiclesProvider vehiclesProvider) : base(locationsProvider, distancesProvider, vehiclesProvider)
        {
        }


        public override void Optimize()
        {
            //TODO: test
            var consideredVehicles = new List<Vehicle>();
            while (Locations.Any())
            {
                if (!consideredVehicles.Any(vhcl => vhcl.TotalLength <= 0))
                {
                    if (Vehicles.Any())
                    {
                        consideredVehicles.Add(Vehicles.First());
                        Vehicles.RemoveAt(0);
                    }
                }
                InsertGreedyLocationVehicleMatch(Locations, consideredVehicles);
            }
            consideredVehicles.ForEach(
                vhcl => vhcl.Optimize());
            Vehicles.AddRange(consideredVehicles);
            if (Vehicles.Sum(vhcl => vhcl.TotalSize) != LocationsProvider.GetLocations().Sum(loc => loc.Size))
                throw new NoSolutionException("Tree clustering did not cluster all locations");
        }

        protected virtual void InsertGreedyLocationVehicleMatch(List<Location> locations, List<Vehicle> consideredVehicles)
        {
            double cheapestInsertion = double.PositiveInfinity;
            Location selectedLocation = null;
            Vehicle selectedVehicle = null;
            foreach (Location location in locations)
            {
                GetChepestVehicleForLocation(consideredVehicles, ref cheapestInsertion, ref selectedLocation, ref selectedVehicle, location);
            }
            if (selectedVehicle != null && selectedLocation != null)
            {
                selectedVehicle.Insert(selectedLocation);
                locations.Remove(selectedLocation);
            }
            else if (locations.Any())
            {
                throw new NoSolutionException("Greedy solution impossible to find");
            }
        }

        protected static void GetChepestVehicleForLocation(List<Vehicle> consideredVehicles, ref double cheapestInsertion, ref Location selectedLocation, ref Vehicle selectedVehicle, Location location)
        {
            var testedEmpty = false;
            foreach (Vehicle vehicle in consideredVehicles)
            {
                if (vehicle.Schedule.Count < 3 && testedEmpty)
                    break;
                double testCost = vehicle.ComputeInsertionCost(location);
                if (testCost < cheapestInsertion)
                {
                    selectedLocation = location;
                    selectedVehicle = vehicle;
                    cheapestInsertion = testCost;
                }
                if (vehicle.Schedule.Count < 3)
                    testedEmpty = true;
            }
        }
    }
}
