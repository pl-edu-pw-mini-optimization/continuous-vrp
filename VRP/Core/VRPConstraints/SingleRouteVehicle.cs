﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Providers;
using VRP.Core.TSPAlgorithms;
using VRP.Core.Locations;

namespace VRP.Core.VRPConstraints
{
    public class SingleRouteVehicle : Vehicle
    {
        public FixedEndsRoute VehiclesRoute { get; private set; }

        public override IReadOnlyCollection<Location> Schedule { get
            {
                return VehiclesRoute.Schedule;
            } }

        public SingleRouteVehicle(
            double capacity,
            Location startDepot,
            Location endDepot,
            IDistancesProvider distancesProvider,
            RouteFactory routeFactory) : base(capacity)
        {
            VehiclesRoute = routeFactory.CreateRoute(startDepot, endDepot, distancesProvider);
            TotalLength = VehiclesRoute.Length;
            TotalSize = VehiclesRoute.TotalSize;
        }

        public override bool Insert(Location location)
        {
            //TODO: test
            if (VehiclesRoute.TotalSize + location.Size > Capacity)
                return false;
            if (VehiclesRoute.TryInsert(location, out double insertionCost))
            {
                TotalLength = VehiclesRoute.Length;
                TotalSize = VehiclesRoute.TotalSize;
                return true;
            }
            return false;
        }

        public override bool Add(Location location)
        {
            //TODO: test
            if (VehiclesRoute.TotalSize + location.Size > Capacity)
                return false;
            VehiclesRoute.Add(location, out double insertionCost);
            TotalLength += insertionCost;
            TotalSize += location.Size;
            return true;
        }

        public override bool Remove(Location location)
        {
            //TODO: test
            if (VehiclesRoute.Remove(location, out double removeCost))
            {
                TotalLength += removeCost;
                TotalSize -= location.Size;
                return true;
            }
            return false;
        }

        public override double ComputeInsertionCost(Location location)
        {
            //TODO: test
            if (VehiclesRoute.TotalSize + location.Size > Capacity)
                return double.PositiveInfinity;
            double insertionCost = VehiclesRoute.ComputeLowestCostInsertionPlace(location, out int index);
            //TODO: cachedLocation = location;
            //TODO: cachedInsertionCost = insertionCost;
            //TODO: cachedIndex = index;
            return insertionCost;
        }

        public override void Optimize()
        {
            VehiclesRoute.Optimize();
            TotalLength = VehiclesRoute.Length;
        }
    }
}
