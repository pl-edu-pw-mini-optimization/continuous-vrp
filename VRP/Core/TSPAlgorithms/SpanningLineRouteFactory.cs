﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core.Locations;
using VRP.Providers;

namespace VRP.Core.TSPAlgorithms
{
    public class SpanningLineRouteFactory : RouteFactory
    {
        public override FixedEndsRoute CreateRoute(Location startPoint, Location endPoint, IDistancesProvider distancesProvider)
        {
            return new SpanningLineRoute(startPoint, endPoint, distancesProvider);
        }
    }
}
