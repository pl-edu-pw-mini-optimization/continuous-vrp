﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core.Locations;
using VRP.Providers;

namespace VRP.Core.TSPAlgorithms
{
    //TODO: Need to write spanning path algorithm - TSPs are quire awfull
    public class NoFurtherOptRoute : FixedEndsRoute
    {
        public NoFurtherOptRoute(Location startPoint, Location endPoint, IDistancesProvider distancesProvider) : base(startPoint, endPoint, distancesProvider)
        {
        }

        public override void Optimize()
        {
            //do nothing by design
        }
    }
}
