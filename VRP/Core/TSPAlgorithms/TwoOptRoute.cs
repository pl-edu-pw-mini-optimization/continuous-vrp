﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core.Locations;
using VRP.Providers;

namespace VRP.Core.TSPAlgorithms
{
    public class TwoOptRoute : FixedEndsRoute
    {
        public TwoOptRoute(Location startPoint, Location endPoint, IDistancesProvider distancesProvider) : base(startPoint, endPoint, distancesProvider)
        {
        }

        /// <summary>
        /// In this class - this is 2OPT
        /// </summary>
        public override void Optimize()
        {
            bool improvementFound = false;
            do
            {
                improvementFound = false;
                double largestImprovement = 1e-4;
                int bestStart = -1;
                int bestEnd = -1;
                for (int start = 0; start < locations.Count - 3; ++start)
                {
                    for (int end = start + 2; end < locations.Count - 1; ++end)
                    {
                        double gain = ComputeGainFromSwappingPoints(start, end);
                        if (gain > largestImprovement)
                        {
                            bestStart = start;
                            bestEnd = end;
                            largestImprovement = gain;
                            improvementFound = true;
                        }
                    }
                }
                if (improvementFound)
                {
                    Length -= largestImprovement;
                    locations.Reverse(bestStart + 1, bestEnd - bestStart);
                }
            } while (improvementFound);

        }

        private double ComputeGainFromSwappingPoints(int start, int end)
        {
            double startEdgeLength = distancesProvider.GetDistance(locations[start], locations[start + 1]);
            double endEdgeLength = distancesProvider.GetDistance(locations[end], locations[end + 1]);
            double newStartEdgeLength = distancesProvider.GetDistance(locations[start], locations[end]);
            double newEndEdgeLength = distancesProvider.GetDistance(locations[start + 1], locations[end + 1]);
            double startToEndDistance = 0.0;
            double reversedStartToEndDistance = 0.0;
            for (int idx = start + 1; idx < end; idx++)
            {
                startToEndDistance += distancesProvider.GetDistance(locations[idx], locations[idx + 1]);
                reversedStartToEndDistance += distancesProvider.GetDistance(locations[idx + 1], locations[idx]);
            }
            double gain = startEdgeLength + endEdgeLength + startToEndDistance
            - newStartEdgeLength - newEndEdgeLength - reversedStartToEndDistance;
            return gain;
        }

    }
}
