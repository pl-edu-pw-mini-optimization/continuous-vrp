﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core.Locations;
using VRP.Providers;

namespace VRP.Core.TSPAlgorithms
{
    public class OpenEndsRoute: Route
    {
        public OpenEndsRoute(IDistancesProvider distancesProvider) : base(distancesProvider)
        {
        }

        public int Insert(Location location)
        {
            var insertCost = FindMinimalCostInsert(location, out int index, 0, locations.Count + 1);
            if (index < 0)
                return index;
            Insert(location, insertCost, index);
            return index;
        }
    }
}
