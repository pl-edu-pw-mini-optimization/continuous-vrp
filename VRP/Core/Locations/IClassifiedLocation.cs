﻿namespace VRP.Core.Locations
{
    internal interface IClassifiedLocation
    {
        long ClassId
        {
            get;
        }
    }
}