﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core.TSPAlgorithms;

namespace VRP.Core.Locations
{
    public abstract class Location
    {
        public long Id { get; protected set; }
        public double X { get; protected set; }
        public double Y { get; protected set; }
        public double Size { get; protected set; }
        public int Rank { get; set; }
        public FixedEndsRoute Route { get; protected set; }

        public Location(long id, double x, double y, double size)
        {
            Id = id;
            X = x;
            Y = y;
            Size = size;
            Rank = Common.Utils.RANDOM.Next();
        }

        public abstract Location Copy();
        internal abstract double InsertToRoute(FixedEndsRoute fixedEndsRoute, int index);
        public abstract void ClearRoute();
    }
}
