﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core.TSPAlgorithms;

namespace VRP.Core.Locations
{
    public class ClassifiedLocation : Location, IClassifiedLocation
    {
        public long ClassId { get; private set; }

        public ClassifiedLocation(long classId, long id, double x, double y, double size) : base(id, x, y, size)
        {
            this.ClassId = classId;
        }

        public override Location Copy()
        {
            return new ClassifiedLocation(ClassId, Id, X, Y, Size);
        }

        internal override double InsertToRoute(FixedEndsRoute fixedEndsRoute, int index)
        {
            this.Route = fixedEndsRoute;
            return fixedEndsRoute.Insert(this, index);
        }

        public override void ClearRoute()
        {
            this.Route = null;
        }
    }
}
