﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core.TSPAlgorithms;

namespace VRP.Core.Locations
{
    public class SimpleLocation : Location
    {
        public SimpleLocation(long id, double x, double y, double size) : base(id, x, y, size)
        {
        }

        public override Location Copy()
        {
                Location copyLocation = new SimpleLocation(this.Id, this.X, this.Y, this.Size);
                return copyLocation;
        }

        public override void ClearRoute()
        {
            this.Route = null;
        }

        internal override double InsertToRoute(FixedEndsRoute fixedEndsRoute, int index)
        {
            this.Route = fixedEndsRoute;
            return fixedEndsRoute.Insert(this, index);
        }
    }
}
