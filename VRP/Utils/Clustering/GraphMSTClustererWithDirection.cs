﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VRP.Utils.Clustering
{
    public class GraphMSTClustererWithDirection : GraphMSTClusterer
    {
        public List<ClusteredLocation> InLocations { get; private set; }
        public List<ClusteredLocation> OutLocations { get; private set; }

        public GraphMSTClustererWithDirection(double capacity, List<ClusteredLocation> clusteredLocations, IOrderedEnumerable<Edge> edges)
            : base(capacity, clusteredLocations, edges)
        {
        }

        protected override bool CanJoinClusters(double capacity, Edge e)
        {
            return (
                !this.OutLocations.Contains(e.locationFrom) &&
                !this.InLocations.Contains(e.locationTo) &&
                base.CanJoinClusters(capacity, e));
        }
        protected override void RegisterNewEdge(Edge e)
        {
            base.RegisterNewEdge(e);
            this.InLocations.Add(e.locationTo);
        }

        protected override void InitializeFields(List<ClusteredLocation> clusteredLocations)
        {
            base.InitializeFields(clusteredLocations);
            this.InLocations = new List<ClusteredLocation>();
            this.OutLocations = new List<ClusteredLocation>();
            this.InLocations.Add(clusteredLocations.First());
            this.OutLocations.Add(clusteredLocations.Last());
        }
    }
}
