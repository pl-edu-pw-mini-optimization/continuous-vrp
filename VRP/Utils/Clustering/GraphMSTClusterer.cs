﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRP.Providers;

namespace VRP.Utils.Clustering
{
    public class GraphMSTClusterer
    {
        public List<Cluster> Clusters { get; private set; }
        public List<Edge> EdgesOutput { get; private set; }

        public static IOrderedEnumerable<Edge> ComputeOrderedEdges(IDistancesProvider provider, List<ClusteredLocation> locationsFrom, List<ClusteredLocation> locationsTo)
        {
            List<Edge> edges = new List<Edge>();
            foreach (ClusteredLocation locationFrom in locationsFrom)
            {
                foreach (ClusteredLocation locationTo in locationsTo)
                {
                    if (locationFrom.Id != locationTo.Id)
                    {
                        Edge e = new Edge()
                        {
                            locationFrom = locationFrom,
                            locationTo = locationTo,
                            distance = provider.GetDistance(locationFrom, locationTo),
                        };
                        edges.Add(e);
                    }
                }
            }
            return edges
                .OrderBy(e => e.distance);
        }

        public GraphMSTClusterer(double capacity, List<ClusteredLocation> clusteredLocations, IOrderedEnumerable<Edge> edges)
        {
            InitializeFields(clusteredLocations);
            foreach (ClusteredLocation clusteredLocation in clusteredLocations)
            {
                Cluster c = new Cluster();
                c.Add(clusteredLocation);
                Clusters.Add(c);
            }
            foreach (Edge e in edges)
            {
                if (e.locationTo.cluster != e.locationFrom.cluster)
                {
                    if (CanJoinClusters(capacity, e))
                    {
                        RegisterNewEdge(e);
                        Cluster toBeRemoved = e.locationTo.cluster;
                        foreach (ClusteredLocation location in e.locationTo.cluster.Locations)
                        {
                            e.locationFrom.cluster.Add(location);
                        }
                        Clusters.Remove(toBeRemoved);
                    }
                }
            }
        }

        protected virtual void InitializeFields(List<ClusteredLocation> clusteredLocations)
        {
            Clusters = new List<Cluster>();
            EdgesOutput = new List<Edge>();
        }

        protected virtual void RegisterNewEdge(Edge e)
        {
            EdgesOutput.Add(e);
        }

        protected virtual bool CanJoinClusters(double capacity, Edge e)
        {
            return e.locationTo.cluster.Size + e.locationFrom.cluster.Size <= capacity;
        }
    }
}
