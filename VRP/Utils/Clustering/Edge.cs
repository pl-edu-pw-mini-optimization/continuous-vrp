﻿namespace VRP.Utils.Clustering
{
    public struct Edge
    {
        public ClusteredLocation locationFrom;
        public ClusteredLocation locationTo;
        public double distance;
    }
}