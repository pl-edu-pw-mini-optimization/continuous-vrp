﻿using System;
using VRP.Core;
using VRP.Core.Locations;
using VRP.Core.TSPAlgorithms;

namespace VRP.Utils.Clustering
{
    public class ClusteredLocation : Location
    {
        public Location Location { get; private set; }
        public Cluster cluster;

        public ClusteredLocation(Location location) : base(location.Id, location.X, location.Y, location.Size)
        {
            this.Location = location;
        }

        public override Location Copy()
        {
            return new ClusteredLocation(this.Location.Copy());
        }

        internal override double InsertToRoute(FixedEndsRoute fixedEndsRoute, int index)
        {
            this.Route = fixedEndsRoute;
            return fixedEndsRoute.Insert(this, index);
        }

        public override void ClearRoute()
        {
            this.Route = null;
            Location.ClearRoute();
        }
    }
}