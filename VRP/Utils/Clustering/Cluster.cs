﻿using System.Collections.Generic;
using System.Linq;

namespace VRP.Utils.Clustering
{
    public class Cluster
    {
        public List<ClusteredLocation> Locations { get; private set; } = new List<ClusteredLocation>();
        public double Size { get { return Locations.Sum(loc => loc.Size); } }

        public void Add(ClusteredLocation location)
        {
            Locations.Add(location);
            location.cluster = this;
        }
    }
}