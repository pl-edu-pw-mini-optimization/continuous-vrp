﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;
using VRP.Providers;
using VRP.Core.Locations;

namespace ContVRP
{
    public class ContinuousVRPWithExplicitCapacityClusteringAndDistance : ContinuousVRPWithExplicitCapacityClustering
    {
        protected override string Type => "V3";

        public ContinuousVRPWithExplicitCapacityClusteringAndDistance(int swarmSize, int iterations, int kClustersPerVehicle, List<VRP.Core.VRPAlgorithms.VRPAlgorithm> heuristicOptimizers, IAllDataProvider provider)
            : base(swarmSize, iterations, kClustersPerVehicle, heuristicOptimizers, provider)
        {
        }

        protected ContinuousVRPWithExplicitCapacityClusteringAndDistance(int swarmSize, int iterations, int kClustersPerVehicle, List<VRP.Core.VRPAlgorithms.VRPAlgorithm> heuristicOptimizers, IAllDataProvider provider, Dictionary<int, Location> frozenLocations):
            base(swarmSize, iterations, kClustersPerVehicle, heuristicOptimizers, provider, frozenLocations)
        {
        }


        protected override void ClusterLocations(out List<Location> unassignedLocations, List<ContinuousCluster> clusters, List<Location> locationsToCluster)
        {
            Dictionary<ContinuousCluster, List<ContinuousCluster>> clusterGroups;
            Dictionary<List<ContinuousCluster>, double> clusterGropusSums;
            PrepareClusterGroups(clusters, locationsToCluster, out clusterGroups, out clusterGropusSums);
            double capacity;
            IOrderedEnumerable<Location> orderedLocationsToCluster;
            IOrderedEnumerable<ContinuousCluster> activeClusters;
            InitializeObjectsForClusterLocations(out unassignedLocations, clusters, locationsToCluster, out capacity, out orderedLocationsToCluster, out activeClusters);
            Dictionary<ContinuousCluster, KeyValuePair<Location, double>> closestLocations = new Dictionary<ContinuousCluster, KeyValuePair<Location, double>>();
            Dictionary<Location, KeyValuePair<ContinuousCluster, double>> closestClusters = new Dictionary<Location, KeyValuePair<ContinuousCluster, double>>();
            List<Location> seeds = SelectSeeds(orderedLocationsToCluster, activeClusters, closestLocations, closestClusters);

            foreach (Location location in orderedLocationsToCluster)
            {
                double minDistance = double.PositiveInfinity;
                ContinuousCluster selectedCluster = null;
                //a tu jedziemy po seedach
                foreach (Location seed in seeds)
                {
                    //tutaj jest istota sprawy
                    double testDistance = Math.Min(DistancesProvider.GetDistance(seed, location),
                        DistancesProvider.GetDistance(location, seed));
                    var cluster = closestClusters[seed].Key;
                    SelectionRule(clusterGroups, clusterGropusSums, capacity, location, ref minDistance, ref selectedCluster, cluster, testDistance);
                }
                InsertLocationIntoSelectedCluster(unassignedLocations, clusterGroups, clusterGropusSums, location, selectedCluster);
            }
        }

        private static List<Location> SelectSeeds(IOrderedEnumerable<Location> orderedLocationsToCluster, IOrderedEnumerable<ContinuousCluster> activeClusters, Dictionary<ContinuousCluster, KeyValuePair<Location, double>> closestLocations, Dictionary<Location, KeyValuePair<ContinuousCluster, double>> closestClusters)
        {
            foreach (Location location in orderedLocationsToCluster)
            {
                foreach (ContinuousCluster cluster in activeClusters)
                {
                    double testDistance = Common.Utils.GetDistanceBewteenCoordinates(cluster.Y, cluster.X, location.Y, location.X);
                    if (!closestLocations.ContainsKey(cluster))
                    {
                        closestLocations.Add(cluster, new KeyValuePair<Location, double>(location, testDistance));
                    }
                    else if (closestLocations[cluster].Value > testDistance)
                    {
                        closestLocations[cluster] = new KeyValuePair<Location, double>(location, testDistance);
                    }
                    if (!closestClusters.ContainsKey(location))
                    {
                        closestClusters.Add(location, new KeyValuePair<ContinuousCluster, double>(cluster, testDistance));
                    }
                    else if (closestClusters[location].Value > testDistance)
                    {
                        closestClusters[location] = new KeyValuePair<ContinuousCluster, double>(cluster, testDistance);
                    }
                }
            }
            var selectedClusters = closestClusters.Select(loc => loc.Value.Key).Distinct();
            List<Location> seeds = new List<Location>();
            foreach (ContinuousCluster selCluster in selectedClusters)
            {
                seeds.Add(closestLocations[selCluster].Key);
            }

            return seeds;
        }

        public override IFunction CreateCopy()
        {
            return new ContinuousVRPWithExplicitCapacityClusteringAndDistance(this.swarmSize, this.iterations, this.kClustersPerVehicle, this.heuristicOptimizers, AllProvider, frozenLocations);
        }


    }
}
