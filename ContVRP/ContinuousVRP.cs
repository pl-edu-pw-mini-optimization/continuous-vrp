﻿using ParticleSwarmOptimization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRP.Core;
using VRP.Core.Locations;
using VRP.Core.TSPAlgorithms;
using VRP.Core.VRPAlgorithms;
using VRP.Providers;

namespace ContVRP
{
    public class ContinuousVRP : BaseContinuousVRP
    {
        public ContinuousVRP(int swarmSize, int iterations, int kClustersPerVehicle, List<VRP.Core.VRPAlgorithms.VRPAlgorithm> heuristicOptimizers, IAllDataProvider provider)
            : base(swarmSize, iterations, kClustersPerVehicle, heuristicOptimizers, provider)
        {
        }

        protected ContinuousVRP(int swarmSize, int iterations, int kClustersPerVehicle, List<VRP.Core.VRPAlgorithms.VRPAlgorithm> heuristicOptimizers, IAllDataProvider provider, Dictionary<int, Location> frozenLocations):
            base(swarmSize, iterations, kClustersPerVehicle, heuristicOptimizers, provider, frozenLocations)
        {
        }

        protected override string Type => "V1";

        public override IFunction CreateCopy()
        {
            return new ContinuousVRP(this.swarmSize, this.iterations, this.kClustersPerVehicle, this.heuristicOptimizers, AllProvider, frozenLocations);
        }

        protected override void ClusterLocations(out List<Location> unassignedLocations, List<ContinuousCluster> clusters, List<Location> locationsToCluster)
        {
            unassignedLocations = new List<Location>();
            foreach (KeyValuePair<int,Location> idedLocation in frozenLocations)
            {
                if (locationsToCluster.RemoveAll(loc => idedLocation.Value.Id == loc.Id) > 0)
                {
                    clusters[idedLocation.Key].Add(idedLocation.Value);
                }
            }
            foreach (Location location in locationsToCluster)
            {
                double minDistance = double.PositiveInfinity;
                ContinuousCluster selectedCluster = null;
                foreach (ContinuousCluster cluster in clusters)
                {
                    double testDistance = (cluster.X - location.X) * (cluster.X - location.X) +
                        (cluster.Y - location.Y) * (cluster.Y - location.Y);
                    if (testDistance < minDistance)
                    {
                        selectedCluster = cluster;
                        minDistance = testDistance;
                    }
                }
                selectedCluster.Add(location);
            }
        }
    }
}
